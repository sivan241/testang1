import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TstComponentComponent } from './tst-component.component';

describe('TstComponentComponent', () => {
  let component: TstComponentComponent;
  let fixture: ComponentFixture<TstComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TstComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TstComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
